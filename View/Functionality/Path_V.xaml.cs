﻿using ViewModel.Path;
using System.Windows.Controls;

namespace View.Functionality
{
    /// <summary>
    /// Логика взаимодействия для Path_V.xaml
    /// </summary>
    public partial class Path_V : UserControl
    {
        public Path_V()
        {
            InitializeComponent();
            DataContext = new PathViewModel();
        }
    }
}
