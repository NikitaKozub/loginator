﻿using View.Functionality;
using View.Functionality.ProgramWindows;
using ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace View.Main
{
    /// <summary>
    /// Логика взаимодействия для GeneratorLP.xaml
    /// </summary>
    public partial class GeneratorLP : Window
    {
        public GeneratorLP()
        {
            InitializeComponent();
        }

        private void Grid_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                DragMove();
            }
        }

        private void ListViewMenu_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int index = ListViewMenu.SelectedIndex;
            MoveCuursorMenu(index);

            switch (index)
            {
                case 0:
                    GridPrincipial.Children.Clear();
                    GridPrincipial.Children.Add(new Path_V());
                    break;
                case 1:
                    GridPrincipial.Children.Clear();
                    GridPrincipial.Children.Add(new LP_Programm_V());
                    break;
                case 2:
                    GridPrincipial.Children.Clear();
                    GridPrincipial.Children.Add(new LP_ProgrammDG_V());
                    break;

                default:
                    break;
            }
        }

        private void MoveCuursorMenu(int index)
        {
            TransitionContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, 60 * index, 0, 0);
        }
    }
}

