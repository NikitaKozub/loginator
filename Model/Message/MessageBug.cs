﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator.Model.Message
{
    /// <summary>
    /// Сообщения об ошибках во всем приложение
    /// </summary>
    public static class MessageBug
    {
        public static List<string> Message;

        public enum message
        {
            Не_правильно_заполнены_данные_дата_в_файле_excel,
            Не_хватает_данных_в_файле_excel,
            Проблема_с_путем_к_файлу_или_папке,
            Не_все_поля_на_экране_заполнены_или_заполнены_корректно
        }
        private static string[] listErrors;


        static MessageBug()
        {
            Message = new List<string>(4);
            listErrors = new string[13];
            listErrors[0] = "Не правильно заполнены данные дата в файле excel";
            listErrors[1] = "Не хватает данных в файле excel";
            listErrors[2] = "Проблема с путем к файлу или папке";
            listErrors[3] = "Не все поля на экране заполнены или заполнены корректно";
        }

        /// <summary>
        /// Добавляет новые сообщения
        /// </summary>
        /// <param name="newMessage">Сообщение</param>
        public static void AddMessage(message newMessage, string additionalMessage)
        {
            Message.Add(listErrors[(int)newMessage] + ": \n -" + additionalMessage);
        }

        /// <summary>
        /// Вовращает все сообщения об ошибках
        /// </summary>
        /// <returns></returns>
        public static List<string> GetMessages()
        {
            return Message;
        }

        /// <summary>
        /// Очищает лист с сообщениями
        /// </summary>
        public static void ClearMessages()
        {
            Message.Clear();
        }
    }
}
