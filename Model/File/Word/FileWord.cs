﻿using Loginator.Model.Data;
using Loginator.Model.Message;
using Loginator2.Model.DataBase.Context;
using Loginator2.Model.DataBase.Model;
using System.Collections.Generic;
using Xceed.Document.NET;
using Xceed.Words.NET;
using Table = Xceed.Document.NET.Table;

namespace Loginator.Model.File.Word
{
    public class FileWord
    {
        private const int COLUMN_REGISTER = 5;//Количество колонок в таблице
        Table TableRegister;
        public int SizeIndentationHanging;// Размер отступа в таблице
        private StudentRecord[] Records;
        public Credentials Credentials;
        public int PositionTable;
        public string FullNameCourse;
        public string DateStartEducation;
        public string DateEndEducation;
        Context Context_;

        public FileWord(Credentials credentials, StudentRecord[] record, string dateStartEducation, string dateEndEducation, Programs programs)
        {
            PositionTable = 199;
            SizeIndentationHanging = 300;
            Records = record;
            Credentials = credentials;

            Context_ = new Context();
            Programs Programm = programs;
            FullNameCourse = Programm.fullName;
            DateStartEducation = dateStartEducation;
            DateEndEducation = dateEndEducation;
        }

        public FileWord(Credentials credentials, StudentRecord[] record, string dateStartEducation, string dateEndEducation, ProgramDGs programs)
        {
            PositionTable = 199;
            SizeIndentationHanging = 300;
            Records = record;
            Credentials = credentials;

            Context_ = new Context();
            ProgramDGs Programm = programs;
            FullNameCourse = Programm.fullName;
            DateStartEducation = dateStartEducation;
            DateEndEducation = dateEndEducation;
        }

        /// <summary>
        /// Настройка колонок таблицы
        /// </summary>
        private void SettingStatement()
        {
            TableRegister.Design = TableDesign.TableGrid;

            TableRegister.Rows[0].Cells[0].Paragraphs[0].Append("№").IndentationHanging = SizeIndentationHanging;
            TableRegister.Rows[0].Cells[0].Width = 18;
            TableRegister.Rows[0].Cells[1].Paragraphs[0].Append("ФИО").IndentationHanging = SizeIndentationHanging;
            TableRegister.Rows[0].Cells[1].Width = 280;
            TableRegister.Rows[0].Cells[2].Paragraphs[0].Append("Login").IndentationHanging = SizeIndentationHanging;
            TableRegister.Rows[0].Cells[2].Width = 300;
            TableRegister.Rows[0].Cells[3].Paragraphs[0].Append("Password").IndentationHanging = SizeIndentationHanging;
            TableRegister.Rows[0].Cells[3].Width = 300;
            TableRegister.Rows[0].Cells[4].Paragraphs[0].Append("Наименование курса").IndentationHanging = SizeIndentationHanging;
            TableRegister.Rows[0].Cells[4].Width = 220;
        }

        /// <summary>
        /// Создание таблицы со списком студентов
        /// </summary>
        /// <returns></returns>
        private Table CreateTable()
        {
            for (int row = 1; row <= Records.Length; row++)
            {
                TableRegister.Rows[row].Cells[0].Paragraphs[0].IndentationHanging = SizeIndentationHanging;
                TableRegister.Rows[row].Cells[0].Width = 25;
                TableRegister.Rows[row].Cells[0].Paragraphs[0].Append(row.ToString()).FontSize(11);

                TableRegister.Rows[row].Cells[1].Paragraphs[0].IndentationHanging = SizeIndentationHanging;
                TableRegister.Rows[row].Cells[1].Width = 220;

                TableRegister.Rows[row].Cells[1].Paragraphs[0].Append(Records[row - 1].GetOneStudent()["Фамилия"] + " " + " " + Records[row - 1].GetOneStudent()["Имя"] + " " + Records[row - 1].GetOneStudent()["Отчество"]).FontSize(11);

                TableRegister.Rows[row].Cells[2].Paragraphs[0].IndentationHanging = SizeIndentationHanging;
                TableRegister.Rows[row].Cells[2].Paragraphs[0].Append(Credentials.Login[row - 1].ToString()).FontSize(11);

                TableRegister.Rows[row].Cells[3].Paragraphs[0].IndentationHanging = SizeIndentationHanging;
                TableRegister.Rows[row].Cells[3].Paragraphs[0].Append(Credentials.Password[row - 1].ToString()).FontSize(11);

                TableRegister.Rows[row].Cells[4].Paragraphs[0].Append(FullNameCourse).FontSize(11);
            }
            return TableRegister;
        }


        /// <summary>
        /// Создание документа ведомость
        /// </summary>
        /// <param name="records"></param>
        public List<string> DocumentCreate(string pathFile, string pathFolderResult)
        {
            using (var document = DocX.Load(pathFile/*Properties.Settings.Default.PathInstructionFile*/))
            {
                TableRegister = document.AddTable(Records.Length + 1, COLUMN_REGISTER);
                SettingStatement();

                document.InsertTable(PositionTable, CreateTable());

                var NameProgrammBookmark = document.Bookmarks["НачалоОбучения"];
                NameProgrammBookmark.SetText(DateStartEducation);

                var IdGroupBookmark = document.Bookmarks["КонецОбучения"];
                IdGroupBookmark.SetText(DateEndEducation);

                var IdCoursBookmark = document.Bookmarks["Курс"];
                IdCoursBookmark.SetText(FullNameCourse);

                document.SaveAs(pathFolderResult/*Loginator2.Properties.Settings.Default.PathFolderResult*/ + "\\Инструкция" + Records[0].GetOneStudent()["Группа"] + "-группы.doc");
            }
            return MessageBug.GetMessages();
        }
    }
}
