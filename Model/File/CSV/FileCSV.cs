﻿using CsvHelper;
using Loginator.Model.Data;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Text;

namespace Loginator.Model.File.Csv
{
    public class FileCSV
    {
        /// <summary>
        /// Заполняет файл
        /// </summary>
        /// <param name="credentials"></param>
        /// <param name="studentRecords"></param>
        /// <param name="GeneratorWindow"></param>
        /// <returns></returns>
        public List<object> FillInFile(Credentials credentials, StudentRecord[] studentRecords, string course)
        {
            var records = new List<object>();
            int line = 2;//1-название столбца, строки начинаются с 1
            for (; line <= studentRecords.Length + 1; line++)
            {
                dynamic record = new ExpandoObject();
                record.username = credentials.Login[line - 2];
                record.password = credentials.Password[line - 2];
                record.lastname = studentRecords[line - 2].GetOneStudent()["Фамилия"].ToString();
                record.firstname = studentRecords[line - 2].GetOneStudent()["Имя"].ToString() + " " + studentRecords[line - 2].GetOneStudent()["Отчество"].ToString();
                record.email = studentRecords[line - 2].GetOneStudent()["Группа"].ToString() + "@none.ru";
                record.course1 = course;
                record.group1 = studentRecords[line - 2].GetOneStudent()["Группа"].ToString();

                records.Add(record);
            }
            return records;
        }

        /// <summary>
        /// Создает csv файл
        /// </summary>
        /// <param name="path">Путь файла</param>
        /// <param name="name">Название файла</param>
        /// <param name="record">Данные в файле</param>
        public void CreateFile(string path, Credentials credentials, StudentRecord[] studentRecords, string couse)
        {
            List<object> dataFile = FillInFile(credentials, studentRecords, couse);
            string fileName = "\\" + "ЛогинПароль" + "-" + studentRecords[0].GetOneStudent()["Группа"] + ".csv";
            FileStream fs = null;
            fs = new FileStream(path + fileName, FileMode.Create);

            using (StreamWriter writer = new StreamWriter(fs, Encoding.UTF8))
            {
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(dataFile);
                }
            }
        }
    }
}
