﻿using Loginator.Model.Data;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using Loginator2.Model.DataBase.Context;
using Context = Loginator2.Model.DataBase.Context.Context;
using Loginator.Model.Message;
using Loginator.Model.Data.Spec;
using System.Data.Entity;
using Loginator2.Model.DataBase.Model;
using Loginator2.Model.DataBase.Model.Program;

namespace Loginator2.Model.File.Excel.Document
{
    /// <summary>
    /// Класс журнал
    /// </summary>
    public class Journal : ExcelDocument
    {
        public string FullNameCourse;
        public string DateStartEducation;
        public string DateEndEducation;
        public string Clock;
        public string Lesson;

        public Journal(StudentRecord[] record, IProgram programm, string dateStartEducation, string dateEndEducation) : base(record, programm)
        {
            Name = "Журнал";
            FullNameCourse = programm.fullName;
            DateStartEducation = dateStartEducation;
            DateEndEducation = dateEndEducation;
            Lesson = programm.Lesson.name;
            Clock = programm.Clock.count.ToString();
        }

        public void Create()
        {
            const byte LIST_EXCEL1 = 1;
            byte idLineGroupTitulPage = 17;
            byte idLineDataTitulPage = 32;
            byte idLineFullNameTitulPage = 20;
            Position_.Add(idLineGroupTitulPage, 6, Record[0].GetOneStudent()["Группа"], LIST_EXCEL1);
            Position_.Add(idLineFullNameTitulPage, 2, FullNameCourse, LIST_EXCEL1);
            Position_.Add(idLineDataTitulPage, 3, "'" + spec.GetNumberData(DateStartEducation, 1) + "'", LIST_EXCEL1);
            Position_.Add(idLineDataTitulPage, 4, spec.GetNumberData(DateStartEducation, 2), LIST_EXCEL1);

            Position_.Add(idLineDataTitulPage, 6, "'" + spec.GetNumberData(DateEndEducation, 1) + "'", LIST_EXCEL1);
            Position_.Add(idLineDataTitulPage, 7, spec.GetNumberData(DateEndEducation, 2), LIST_EXCEL1);
            Position_.Add(idLineDataTitulPage, 9, spec.GetNumberData(DateEndEducation, 3), LIST_EXCEL1);

            const byte LIST_EXCEL2 = 2;
            byte idLineStudent = 5;
            byte idColumnNumber = 1;
            byte idColumnFIO = 2;
            byte idColumnDataBirth = 4;
            byte idColumnSpecialty = 3;
            for (int i = 0; i < Record.Length; i++)
            {
                Position_.Add(idLineStudent, idColumnNumber, (i + 1).ToString(), LIST_EXCEL2);
                Position_.Add(idLineStudent, idColumnFIO, Record[i].GetOneStudent()["Фамилия"] + " " + Record[i].GetOneStudent()["Имя"] + " " + Record[i].GetOneStudent()["Отчество"], LIST_EXCEL2);
                Position_.Add(idLineStudent, idColumnDataBirth, Record[i].GetOneStudent()["ДатаРождения"], LIST_EXCEL2);
                Position_.Add(idLineStudent, idColumnSpecialty, Record[i].GetOneStudent()["Специальность"], LIST_EXCEL2);
                idLineStudent++;
            }

            const byte LIST_EXCEL3 = 3;
            idLineStudent = 5;
            byte idColumnCompany = 1;
            for (int i = 0; i < Record.Length; i++)
            {
                Position_.Add(idLineStudent, idColumnCompany, Record[i].GetOneStudent()["Компания"], LIST_EXCEL3);
                idLineStudent++;
            }

            const byte LIST_EXCEL4 = 4;
            byte idLineDataCursa = 5;
            byte idColumnDataCoursaFullNameCourse = 2;
            byte idColumnDataCoursaClock = 3;
            Position_.Add(idLineDataCursa, idColumnDataCoursaFullNameCourse, FullNameCourse, LIST_EXCEL4);
            Position_.Add(idLineDataCursa, idColumnDataCoursaClock, Clock, LIST_EXCEL4);

            const byte LIST_EXCEL5 = 5;
            string[] fio = spec.CutFromStringElements(Lesson, '\n');
            for (int i = 0; i < fio.Length; i++)
            {
                Position_.Add((byte)(5 + i), 3, fio[i], LIST_EXCEL5);
            }
            byte idLineStartEducation= 5;
            byte idLineEndEducation = 5;
            Position_.Add(idLineStartEducation, 2, DateStartEducation, LIST_EXCEL5);
            Position_.Add((byte)(idLineEndEducation + fio.Length - 1), 2, DateEndEducation, LIST_EXCEL5);

            const byte LIST_EXCEL6 = 6;
            idLineStudent = 3;
            for (int i = 0; i < Record.Length; i++)
            {
                Position_.Add(idLineStudent, idColumnNumber, (i + 1).ToString(), LIST_EXCEL6);
                idLineStudent++;
            }
        }
    }
}
