﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator2.Model.File.Excel.Document
{
    /// <summary>
    /// Позиция в excel документе
    /// </summary>
    public class Position
    {
        public byte Line; //строка
        public byte Column; //столбец
        public string Value; //значение
        public byte ExcelWorksheet; //номер листа
        public List<Position> Positions;

        public Position()
        {
            Positions = new List<Position>();
        }

        public void Add(byte line, byte column, string value, byte excelWorksheet)
        {
            Position position = new Position()
            {
                Line = line,
                Column = column,
                Value = value,
                ExcelWorksheet = excelWorksheet
            };

            Positions.Add(position);
        }
    }
}
