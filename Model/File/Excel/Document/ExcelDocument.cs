﻿using Loginator.Model.Data;
using Loginator.Model.Data.Spec;
using Loginator2.Model.DataBase.Model.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator2.Model.File.Excel.Document
{
    public class ExcelDocument
    {
        public StudentRecord[] Record;
        public SpecFunction spec;
        public Position Position_;
        public IProgram Programm;
        public string Name;

        public ExcelDocument(StudentRecord[] record, IProgram programm)
        {
            Record = record;
            spec = new SpecFunction();
            Position_ = new Position();
            Programm = programm;
        }
    }
}
