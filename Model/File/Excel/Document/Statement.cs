﻿using Loginator.Model.Data;
using Loginator2.Model.DataBase.Context;
using Loginator2.Model.DataBase.Model;
using Loginator2.Model.DataBase.Model.Program;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator2.Model.File.Excel.Document
{
    public class Statement : ExcelDocument
    {
        public string FullNameCourse;
        public string DateStartEducation;
        public string DateEndEducation;
        public string Clock;

        public Statement(StudentRecord[] record, IProgram programm, string dateStartEducation, string dateEndEducation) : base(record, programm)
        {
            Name = "Ведомость";
            FullNameCourse = programm.fullName;
            DateStartEducation = dateStartEducation;
            DateEndEducation = dateEndEducation;
            Clock = programm.Clock.count.ToString();
        }

        public void Create()
        {
            byte ListExcel1 = 1;

            byte idLineStudent = 7;
            byte idColumnNumber = 1;
            byte idColumnFIO = 2;
            byte idColumnDataBirth = 3;
            byte idColumnSpecialty = 4;
            byte idColumnCompany = 5;
            for (int i = 0; i < Record.Length; i++)
            {
                Position_.Add(idLineStudent, idColumnNumber, (i + 1).ToString(), ListExcel1);
                Position_.Add(idLineStudent, idColumnFIO, Record[i].GetOneStudent()["Фамилия"] + " " + Record[i].GetOneStudent()["Имя"] + " " + Record[i].GetOneStudent()["Отчество"], ListExcel1);
                Position_.Add(idLineStudent, idColumnDataBirth, Record[i].GetOneStudent()["ДатаРождения"], ListExcel1);
                Position_.Add(idLineStudent, idColumnSpecialty, Record[i].GetOneStudent()["Специальность"], ListExcel1);
                Position_.Add(idLineStudent, idColumnCompany, Record[i].GetOneStudent()["Компания"], ListExcel1);
                idLineStudent++;
            }
            byte idLineGroupAndNameCours = 2;
            byte idColumnGroup = 1;
            byte idColumnNameCours = 2;
            byte idLineClock = 4;
            byte idColumnClock = 2;
            byte idLineDateStartEducation = 5;
            byte idColumnDateStartEducation = 3;
            byte idLineDateEndEducation = 5;
            byte idColumnDateEndEducation = 4;
            Position_.Add(idLineGroupAndNameCours, idColumnGroup, "Гр № " + Record[0].GetOneStudent()["Группа"], ListExcel1);
            Position_.Add(idLineGroupAndNameCours, idColumnNameCours, FullNameCourse, ListExcel1);
            Position_.Add(idLineClock, idColumnClock, "(Объем самостоятельной работы - " + Clock + " часа)", ListExcel1);
            Position_.Add(idLineDateStartEducation, idColumnDateStartEducation, DateStartEducation + " по", ListExcel1);
            Position_.Add(idLineDateEndEducation, idColumnDateEndEducation, DateEndEducation, ListExcel1);
        }
    }
}

