﻿using Loginator2.Model.File.Excel.Document;

namespace Loginator.Model.File.Excel
{
    interface IFileExcel
    {
        void ReadFile();
        void Write(ExcelDocument excelDocument);
    }
}
