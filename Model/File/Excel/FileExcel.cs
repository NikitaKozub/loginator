﻿using Loginator.Model.Data;
using Loginator.Model.Data.Spec;
using Loginator.Model.Message;
using Loginator2.Model.DataBase.Context;
using Loginator2.Model.DataBase.Model;
using Loginator2.Model.File.Excel.Document;
using OfficeOpenXml;
using System;
using System.IO;
using System.Linq;

namespace Loginator.Model.File.Excel
{
    /// <summary>
    /// Файл эксель
    /// </summary>
    public class FileExcel : IFileExcel
    {
        /// <summary>
        /// Лист с записями студентов
        /// </summary>
        private StudentRecord[] studentRecords;
        /// <summary>
        /// Количество заполненых строк в файле
        /// </summary>
        private int filledString;
        public FileInfo existingFile;
        public ExcelPackage package;
        public ExcelWorksheet worksheet;
        public SpecFunction spec;
        private string PathFolderResult;

        const int LINE_NUMBER_FROM_WHICH_FILE_IS_FILED = 2;
        public FileExcel(string pathDocuemnt, int Sheet, string pathFolderResult)
        {
            existingFile = new FileInfo(pathDocuemnt);
            package = new ExcelPackage(existingFile);
            worksheet = package.Workbook.Worksheets[Sheet];
            GetLineAndColumn();
            spec = new SpecFunction();
            PathFolderResult = pathFolderResult;
        }
        public FileExcel(string pathDocuemnt, string pathFolderResult)
        {
            existingFile = new FileInfo(pathDocuemnt);
            package = new ExcelPackage(existingFile);
            spec = new SpecFunction();
            PathFolderResult = pathFolderResult;
        }

        public StudentRecord[] GetRecords()
        {
            return studentRecords;
        }

        /// <summary>
        /// Получить количество строк, столбцов в файле
        /// </summary>
        /// <returns></returns>
        public void GetLineAndColumn()
        {
            // 1 строка в файле названия колонок, со 2 начинаются данные
            int fileLine = LINE_NUMBER_FROM_WHICH_FILE_IS_FILED;
            while (worksheet.Cells[fileLine, 1].Value != null)
            {
                fileLine++;
            }
            filledString = fileLine - LINE_NUMBER_FROM_WHICH_FILE_IS_FILED;
            studentRecords = new StudentRecord[filledString];
        }

        /// <summary>
        /// Чтение файла excel в лист
        /// </summary>
        /// <returns></returns>
        public void ReadFile()
        {
            using (package)
            {
                int fileLine = LINE_NUMBER_FROM_WHICH_FILE_IS_FILED;// текущая строка в файле excel
                GetLineAndColumn();
                while (worksheet.Cells[fileLine, 1].Value != null)
                {
                    int fileСolumn = 1;// столбец      
                    StudentRecord record = new StudentRecord();
                    while (worksheet.Cells[1, fileСolumn].Value != null)
                    {
                        if (worksheet.Cells[fileLine, fileСolumn].Value != null)
                        {
                            record.AddPropertyRecord(Convert.ToString(worksheet.Cells[1, fileСolumn].Value), Convert.ToString(worksheet.Cells[fileLine, fileСolumn].Value), fileLine);
                        }
                        else
                        {
                            record.AddPropertyRecord(Convert.ToString(worksheet.Cells[1, fileСolumn].Value), " ", fileLine);
                        }
                        fileСolumn++;
                    }
                    studentRecords[fileLine - LINE_NUMBER_FROM_WHICH_FILE_IS_FILED] = record;
                    fileLine++;
                }
            }
        }
        

        public void Write(ExcelDocument excelDocument)
        {
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                foreach (var itr in excelDocument.Position_.Positions)
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[itr.ExcelWorksheet];
                    worksheet.Cells[itr.Line, itr.Column].Value = itr.Value;
                }
                FileInfo fi = new FileInfo(PathFolderResult + "\\" + excelDocument.Name + "_" + excelDocument.Record[0].GetOneStudent()["Группа"] + ".xlsx");
                package.SaveAs(fi);
            }
        }
    }
}
