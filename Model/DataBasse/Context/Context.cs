using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Loginator2.Model.DataBase.Model;

namespace Loginator2.Model.DataBase.Context
{
    public partial class Context : DbContext
    {
        public Context()
            : base("name=DataBaseContext")
        {
        }

        public virtual DbSet<Clock> Clock { get; set; }
        public virtual DbSet<Lesson> Lesson { get; set; }
        public virtual DbSet<ProgramDGs> ProgramDGs { get; set; }
        public virtual DbSet<Programs> Programs { get; set; }
        public virtual DbSet<Students> Students { get; set; }
        public virtual DbSet<TypeDocument> TypeDocument { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Clock>()
                .HasMany(e => e.ProgramDGs)
                .WithRequired(e => e.Clock)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Clock>()
                .HasMany(e => e.Programs)
                .WithRequired(e => e.Clock)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lesson>()
                .HasMany(e => e.Programs)
                .WithRequired(e => e.Lesson)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypeDocument>()
                .HasMany(e => e.ProgramDGs)
                .WithRequired(e => e.TypeDocument)
                .HasForeignKey(e => e.typeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypeDocument>()
                .HasMany(e => e.Programs)
                .WithRequired(e => e.TypeDocument)
                .HasForeignKey(e => e.typeId)
                .WillCascadeOnDelete(false);
        }
    }
}
