using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Loginator2.Model.DataBase.Model.Program;

namespace Loginator2.Model.DataBase.Model
{    
    public partial class Programs : IProgram
    {
        public long id { get; set; }

        [StringLength(2147483647)]
        public string training { get; set; }

        [StringLength(2147483647)]
        public string name { get; set; }

        public long clockId { get; set; }

        public long lessonId { get; set; }

        public long typeId { get; set; }

        [StringLength(2147483647)]
        public string fullName { get; set; }

        public virtual Clock Clock { get; set; }

        public virtual Lesson Lesson { get; set; }

        public virtual TypeDocument TypeDocument { get; set; }

        /// <summary>
        /// �������� �� ������� ��������� �� � ������� ��������(�����)
        /// </summary>
        /// <param name="context">������ � ���� ������</param>
        /// <param name="fullName">������ ��������</param>
        /// <returns></returns>
        public static bool IsProgramm(Loginator2.Model.DataBase.Context.Context context, string fullName)
        {
            if (context == null || fullName == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            
            var isPrograms = context.Programs.Where(c => c.fullName == fullName.Trim());
            if (isPrograms.Count() > 0)
            {
                return true;
            }
            return false;
        }
    }
}
