﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator2.Model.DataBase.Model.Program
{
    public interface IProgram
    {
        long id { get; }
        string name { get; }
        string fullName { get; }
        Clock Clock { get; }
        Lesson Lesson { get; }
        TypeDocument TypeDocument { get; }
    }
}
