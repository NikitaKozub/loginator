namespace Loginator2.Model.DataBase.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Students
    {
        public long id { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string name { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string surname { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string patronymic { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string dateDirth { get; set; }
    }
}
