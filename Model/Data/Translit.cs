﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator.Model.Data
{
    internal sealed class Translit : ITranslit
    {
        public Dictionary<char, char> vocabulary = new Dictionary<char, char>()
        {
            {'а', 'f'},
            {'б', '7'},
            {'в', 'd'},
            {'г', 'u'},
            {'д', '.'},
            {'е', 't'},
            {'ё', '1'},
            {'ж', '2'},
            {'з', 'p'},
            {'и', 'b'},
            {'й', 'q'},
            {'к', 'r'},
            {'л', 'k'},
            {'м', 'v'},
            {'н', 'y'},
            {'о', 'j'},
            {'п', 'g'},
            {'р', 'h'},
            {'с', 'c'},
            {'т', 'n'},
            {'у', 'e'},
            {'ф', 'a'},
            {'х', '3'},
            {'ц', 'w'},
            {'ч', 'x'},
            {'ш', 'i'},
            {'щ', 'o'},
            {'ъ', '4'},
            {'ы', 's'},
            {'ь', 'm'},
            {'э', '5'},
            {'ю', '6'},
            {'я', 'z'}
        };

        public StringBuilder TranslitToEng(string data)
        {
            StringBuilder result = new StringBuilder();
            foreach (char symbol in data)
            {
                char ss = 'a';
                if (vocabulary.TryGetValue(symbol, out ss))
                {
                    result.Append(ss);
                }
                else
                {
                    result.Append(symbol);
                }
            }
            return result;
        }
    }
}
