﻿using IdentityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator.Model.Data
{
    public class Credentials : ICredentials
    {
        public int Count { get; set; }
        public StringBuilder[] Login { get; set; }
        public StringBuilder[] Password { get; set; }
        Translit translit = new Translit();
        public StudentRecord[] Record;

        public Credentials(StudentRecord[] record)
        {
            this.Count = record.Length;
            this.Login = new StringBuilder[this.Count];
            this.Password = new StringBuilder[this.Count];
            for (int i = 0; i < this.Count; i++)
            {
                this.Login[i] = new StringBuilder();
                this.Password[i] = new StringBuilder();
            }
            Record = record;
        }

        public void GeneratedLogin()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this.Login[i].Append(translit.TranslitToEng(this.Record[i].GetOneStudent()["Фамилия"].Substring(0, this.Record[i].GetOneStudent()["Фамилия"].Length).ToLower() + this.Record[i].GetOneStudent()["ДатаРождения"].Substring(6) + this.Record[i].GetOneStudent()["Группа"] + i));
            }
        }

        public void GeneratedPassword()
        {
            for (int i = 0; i < this.Count; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    this.Password[i].Append(((char)RadomInt()).ToString());
                    if (j == 4)
                    {
                        this.Password[i].Append(this.Record[i].GetOneStudent()["Группа"] + i);
                    }
                }
            }
        }

        public int RadomInt()
        {
            CryptoRandom rnd = new CryptoRandom();

            int value = rnd.Next(65, 90);
            return value;
        }
    }
}
