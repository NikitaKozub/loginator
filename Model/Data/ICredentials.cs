﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator.Model.Data
{
    interface ICredentials
    {
        StringBuilder[] Login { get; set; }
        StringBuilder[] Password { get; set; }

        void GeneratedLogin();
        void GeneratedPassword();
        int RadomInt();
    }
}
