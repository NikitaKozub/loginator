﻿using Loginator.Model.Data.Spec;
using Loginator.Model.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loginator.Model.Data
{
    /// <summary>
    /// Запись об одном студенте
    /// </summary>
    public class StudentRecord
    {
        private Dictionary<string, string> OneStudent;
        public Dictionary<string, string> GetOneStudent() => OneStudent;
        //protected const int DEGGERENCE_BETWEEN_BEGINNING_ARRAY_AND_FILE = 1;
        SpecFunction specFunction;
        public StudentRecord()
        {
            OneStudent = new Dictionary<string, string>();
            specFunction = new SpecFunction();
        }

        /// <summary>
        /// Добавить новую запись
        /// Если ее нет вставить пробел
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="value"></param>
        /// <param name="indexLine">номер строки в файле</param>
        public void AddPropertyRecord(string bookmark, string value, int indexLine)
        {
            if (bookmark == null || value == null)
                return;
            OneStudent.Add(bookmark.Trim(), Check(bookmark, value, indexLine));
        }

        /// <summary>
        /// Удаляет запись
        /// </summary>
        /// <param name="bookmark">Назваание записи</param>
        public void RemoveRecord(string bookmark)
        {
            if (bookmark == null)
                return;
            OneStudent.Remove(bookmark);
        }

        /// <summary>
        /// Проверяет данные
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="value">значение</param>
        /// <returns></returns>
        private string Check(string key, string value, int indexLine)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                value = " ";
                return value;
            }

            if (key.Contains("ДатаРождения") || key.Contains("датаРождения"))
            {
                return CorrectDate(value.Trim(), indexLine);
            }
            if (key.Contains("Группа") || key.Contains("Группа"))
            {
                return CorrectGroup(value.Trim(), indexLine);
            }
            if (key.Contains("ФИО") || key.Contains("Фио") || key.Contains("фио"))
            {
                return CorrectFIO(value.Trim(), indexLine);
            }

            return value.Trim();
        }

        /// <summary>
        /// Корректириует фамилию, имя, отчество
        /// </summary>
        /// <param name="date"></param>
        /// <param name="indexLine"></param>
        /// <returns></returns>
        private string CorrectFIO(string date, int indexLine)
        {
            string[] fio = specFunction.CutFromStringElements(date, ' ');
            if (fio.Length <= 2)
            {
                MessageBug.AddMessage(MessageBug.message.Не_хватает_данных_в_файле_excel, "в строке " + indexLine);
            }
            if (fio.Length == 2)
            {
                AddPropertyRecord("Фамилия", fio[0], indexLine);
                AddPropertyRecord("Имя", fio[1], indexLine);
                AddPropertyRecord("Отчество", " ", indexLine);
                return date;
            }
            if (fio.Length == 1)
            {
                AddPropertyRecord("Фамилия", fio[0], indexLine);
                AddPropertyRecord("Имя", " ", indexLine);
                AddPropertyRecord("Отчество", " ", indexLine);
                RemoveRecord("ФИО");
                return date;
            }
            if (fio.Length == 0)
            {
                AddPropertyRecord("Фамилия", " ", indexLine);
                AddPropertyRecord("Имя", " ", indexLine);
                AddPropertyRecord("Отчество", " ", indexLine);
                RemoveRecord("ФИО");
                return date;
            }
            AddPropertyRecord("Фамилия", fio[0], indexLine);
            AddPropertyRecord("Имя", fio[1], indexLine);
            AddPropertyRecord("Отчество", fio[2], indexLine);
            RemoveRecord("ФИО");
            return date;
        }

        /// <summary>
        /// Поверяет наличие данных (группы)
        /// </summary>
        /// <param name="group">группа</param>
        /// <param name="indexLine">номер строки</param>
        /// <returns></returns>
        private string CorrectGroup(string date, int indexLine)
        {
            if (date.Length > 10)
            {
                return date.Substring(0, 10);
            }
            if (date.Length == 0)
            {
                MessageBug.AddMessage(MessageBug.message.Не_правильно_заполнены_данные_дата_в_файле_excel, "в строке " + indexLine);
                date = "Ошибка в группе";
                return date;
            }
            return date;
        }

        /// <summary>
        /// Поверяет наличие данных (даты)
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private string CorrectDate(string date, int indexLine)
        {
            if (date.Length > 10)
            {
                return date.Substring(0, 10);
            }
            if (date.Length < 10)
            {
                MessageBug.AddMessage(MessageBug.message.Не_правильно_заполнены_данные_дата_в_файле_excel, "в строке " + indexLine);
                date = "01.01.0001";
                return date;
            }
            return date;
        }
    }
}
