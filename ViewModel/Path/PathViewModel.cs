﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using ViewModel.MVVM;

namespace ViewModel.Path
{
    public class PathViewModel
    {
        public PathModel Path { get; set; }

        public PathViewModel()
        {
            DateTime thisDay = DateTime.Today;
            Path = new PathModel();

            SelectPathSourceFile = new RelayCommand(arg => СhoicePathSourceFile());
            SelectPathFolderResult = new RelayCommand(arg => СhoicePathFolderResult());
            SelectPathInstructionFile = new RelayCommand(arg => СhoicePathInstructionFile());
            SelectPathStatementFile = new RelayCommand(arg => СhoicePathStatementFile());
            SelectPathJournalFile = new RelayCommand(arg => СhoicePathJournalFile());
        }

        public ICommand SelectPathSourceFile { get; set; }
        public void СhoicePathSourceFile()
        {
            OpenFileDialog openFileDialog = GetSettingFileExcelDialog("Выберете шаблон исходных данных студентов(excel)");
            openFileDialog.ShowDialog();
            if (openFileDialog.SafeFileName.Length > 0)
            {
                Path.PathSourceFile = openFileDialog.FileName;
            }
        }

        public ICommand SelectPathFolderResult { get; set; }
        public void СhoicePathFolderResult()
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            DialogResult result = folderBrowser.ShowDialog();

            if (!string.IsNullOrWhiteSpace(folderBrowser.SelectedPath))
            {
                Path.PathFolderResult = folderBrowser.SelectedPath;
            }
        }

        public ICommand SelectPathInstructionFile { get; set; }
        public void СhoicePathInstructionFile()
        {
            OpenFileDialog openFileDialog = GetSettingFileWordDialog("Выберете шаблон инструкции(word)");

            openFileDialog.ShowDialog();
            if (openFileDialog.SafeFileName.Length > 0)
            {
                Path.PathInstructionFile = openFileDialog.FileName;
            }
        }

        public ICommand SelectPathStatementFile { get; set; }
        public void СhoicePathStatementFile()
        {
            OpenFileDialog openFileDialog = GetSettingFileExcelDialog("Выберете шаблон ведомости(excel)");
            openFileDialog.ShowDialog();
            if (openFileDialog.SafeFileName.Length > 0)
            {
                Path.PathStatementFile = openFileDialog.FileName;
            }
        }

        public ICommand SelectPathJournalFile { get; set; }
        public void СhoicePathJournalFile()
        {
            OpenFileDialog openFileDialog = GetSettingFileExcelDialog("Выберете шаблон журнала(excel)");
            openFileDialog.ShowDialog();
            if (openFileDialog.SafeFileName.Length > 0)
            {
                Path.PathJournalFile = openFileDialog.FileName;
            }
        }

        /// <summary>
        /// Настройка диалогового окна для выбора Excel файла
        /// </summary>
        /// <returns>Объект с настройками</returns>
        private OpenFileDialog GetSettingFileExcelDialog(string nameWindow)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                DefaultExt = "*.xls;*.xlsx",
                Filter = "Microsoft Excel (*.xls*)|*.xls*",
                Title = nameWindow
            };
            return openFileDialog;
        }

        /// <summary>
        /// Настройка диалогового окна для выбора word файла
        /// </summary>
        /// <returns>Объект с настройками</returns>
        private OpenFileDialog GetSettingFileWordDialog(string nameWindow)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = "*.doc;*.docx";
            openFileDialog.Filter = "Microsoft Word (*.doc*)|*.docx*";
            openFileDialog.Title = nameWindow;
            return openFileDialog;
        }
    }
}
