﻿using Loginator.Model.Data;
using Loginator.Model.File.Csv;
using Loginator.Model.File.Excel;
using Loginator.Model.File.Word;
using Loginator.Model.Message;
using Loginator2.Model.DataBase.Context;
using Loginator2.Model.DataBase.Model;
using Loginator2.Model.File.Excel.Document;
using System;
using System.Data.Entity;
using ViewModel.MVVM;
using ViewModel.MVVM.ProgramWindows;

namespace ViewModel.ProgrammWindows
{
    public class LgProgrammDGViewModel
    {
        public PathModel Path { get; set; }
        public ProgrammDGModel ProgrammWindow { get; set; }
        private FileExcel DateFromFile;
        public Credentials credentials;
        public Context context_;
        public ProgramDGs programs;

        public LgProgrammDGViewModel()
        {
            DateTime thisDay = DateTime.Today;
            Path = new PathModel();
            context_ = new Context();
            context_.ProgramDGs.Load();

            ProgrammWindow = new ProgrammDGModel()
            {
                Course = "Название курса",
                DateStartEducation = thisDay.ToString("d"),
                DateEndEducation = thisDay.ToString("d"),
                Programs = context_.ProgramDGs.Local.ToBindingList()
            };
        }

        /// <summary>
        /// Заполнены ли поля окна
        /// </summary>
        /// <returns></returns>
        private bool IsWindowFieldsFilled()
        {
            if (ProgrammWindow.Course != "Название курса")
            {
                return true;
            }
            MessageBug.AddMessage(MessageBug.message.Не_все_поля_на_экране_заполнены_или_заполнены_корректно, "Проверьте поля с названиями курса");
            return false;
        }

        /// <summary>
        /// Заполнены ли настройки
        /// </summary>
        /// <returns></returns>
        private bool IsTheSettingsFull()
        {
            if (Path.PathSourceFile != "Найди свой путь" &&
                Path.PathFolderResult != "Найди свой путь" &&
                Path.PathStatementFile != "Найди свой путь" &&
                Path.PathInstructionFile != "Найди свой путь" &&
                Path.PathJournalFile != "Найди свой путь")
            {
                return true;
            }
            MessageBug.AddMessage(MessageBug.message.Проблема_с_путем_к_файлу_или_папке, "Проверьте вкладку настройки");
            return false;
        }

        RelayCommand generationLoginAndPassword;
        public RelayCommand GenerationLoginAndPassword
        {
            get
            {
                return generationLoginAndPassword ??
                    (generationLoginAndPassword = new RelayCommand((selectedItem) =>
                    {
                        MessageBug.ClearMessages();
                        programs = selectedItem as ProgramDGs;
                        if (programs == null)
                        {
                            MessageBug.AddMessage(MessageBug.message.Не_все_поля_на_экране_заполнены_или_заполнены_корректно, "Программа не выбрана");
                        }
                        if (IsWindowFieldsFilled() && IsTheSettingsFull() && programs != null)
                        {
                            DateFromFile = new FileExcel(Path.PathSourceFile, 1, Properties.Settings.Default.PathFolderResult);
                            DateFromFile.ReadFile();

                            credentials = new Credentials(DateFromFile.GetRecords());
                            credentials.GeneratedLogin();
                            credentials.GeneratedPassword();
                            CreateFileCSVeResult();
                            CreateFileWordInstruction();
                            CreateFileStatement(DateFromFile.GetRecords());
                            CreateFileJournal(DateFromFile.GetRecords());
                        }
                        string message = " ";
                        foreach (string itr in MessageBug.GetMessages())
                        {
                            message += itr + "\n";
                        }
                        ProgrammWindow.Message = message;
                    }));
            }
        }

        /// <summary>
        /// Создания CSV файла для загрузки в Moodle
        /// </summary>
        public void CreateFileCSVeResult()
        {
            FileCSV fileCSV = new FileCSV();
            if (credentials.Login[0].Length > 0 && credentials.Password[0].Length > 0)
            {
                fileCSV.CreateFile(Path.PathFolderResult, credentials, DateFromFile.GetRecords(), ProgrammWindow.Course);
            }
        }

        /// <summary>
        /// Создания инструкции для пользователей
        /// </summary>
        public void CreateFileWordInstruction()
        {
            FileWord instruction = new FileWord(credentials, DateFromFile.GetRecords(), ProgrammWindow.DateStartEducation, ProgrammWindow.DateEndEducation, programs);
            instruction.DocumentCreate(Properties.Settings.Default.PathInstructionFile,
                                       Properties.Settings.Default.PathFolderResult);
        }

        /// <summary>
        /// Создание ведомостей
        /// </summary>
        /// <param name="studentRecords"></param>
        public void CreateFileStatement(StudentRecord[] studentRecords)
        {
            Statement statement = new Statement(studentRecords, programs, ProgrammWindow.DateStartEducation, ProgrammWindow.DateEndEducation);
            statement.Create();

            FileExcel fileExcelStatement = new FileExcel(Properties.Settings.Default.PathStatementFile,
                                                         Properties.Settings.Default.PathFolderResult);
            fileExcelStatement.Write(statement);
        }

        /// <summary>
        /// Создание журнала группы
        /// </summary>
        /// <param name="studentRecords"></param>
        public void CreateFileJournal(StudentRecord[] studentRecords)
        {
            Journal journal = new Journal(studentRecords, programs, ProgrammWindow.DateStartEducation, ProgrammWindow.DateEndEducation);
            journal.Create();

            FileExcel fileExcelJournal = new FileExcel(Properties.Settings.Default.PathJournalFile,
                                                       Properties.Settings.Default.PathFolderResult);
            fileExcelJournal.Write(journal);
        }
    }
}
