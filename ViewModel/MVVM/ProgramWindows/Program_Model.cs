﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.MVVM.ProgramWindows
{
    public class Program_Model : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Дата начала обучения
        /// </summary>
        private string _DateStartEducation;
        public string DateStartEducation
        {
            get { return _DateStartEducation; }
            set
            {
                if (_DateStartEducation != value)
                {
                    _DateStartEducation = value;
                    OnPropertyChanged("DateStartEducation");
                }
            }
        }

        /// <summary>
        /// Дата начала обучения
        /// </summary>
        private string _DateEndEducation;
        public string DateEndEducation
        {
            get { return _DateEndEducation; }
            set
            {
                if (_DateEndEducation != value)
                {
                    _DateEndEducation = value;
                    OnPropertyChanged("DateEndEducation");
                }
            }
        }

        /// <summary>
        /// Курс
        /// </summary>
        private string _Course;
        public string Course
        {
            get { return _Course; }
            set
            {
                if (_Course != value)
                {
                    _Course = value;
                    OnPropertyChanged("Course");
                }
            }
        }

        /// <summary>
        /// Сообщения
        /// </summary>
        private string _Message;
        public string Message
        {
            get { return _Message; }
            set
            {
                if (_Message != value)
                {
                    _Message = value;
                    OnPropertyChanged("Message");
                }
            }
        }
    }
}
