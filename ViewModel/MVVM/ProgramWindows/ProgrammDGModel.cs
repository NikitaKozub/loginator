﻿using Loginator2.Model.DataBase.Model.Program;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.MVVM.ProgramWindows
{
    public class ProgrammDGModel : Program_Model
    {
        /// <summary>
        /// Список программ из базы данных
        /// </summary>
        private IEnumerable<IProgram> _Programs;
        public IEnumerable<IProgram> Programs
        {
            get { return _Programs; }
            set
            {
                if (_Programs != value)
                {
                    _Programs = value;
                    OnPropertyChanged("Programs");
                }
            }
        }
    }
}
