﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.MVVM
{
    public class PathModel : INotifyPropertyChanged
    {
        public PathModel()
        {
            PathSourceFile = Properties.Settings.Default.PathSourceFile;
            PathFolderResult = Properties.Settings.Default.PathFolderResult;
            PathInstructionFile = Properties.Settings.Default.PathInstructionFile;
            PathStatementFile = Properties.Settings.Default.PathStatementFile;
            PathJournalFile = Properties.Settings.Default.PathJournalFile;
        }

        private string _PathJournalFile;
        public string PathJournalFile
        {
            get { return _PathJournalFile; }
            set
            {
                if (_PathJournalFile != value)
                {
                    _PathJournalFile = value;
                    Properties.Settings.Default.PathJournalFile = _PathJournalFile;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("PathJournalFile");
                }
            }
        }

        private string _PathStatementFile;
        public string PathStatementFile
        {
            get { return _PathStatementFile; }
            set
            {
                if (_PathStatementFile != value)
                {
                    _PathStatementFile = value;
                    Properties.Settings.Default.PathStatementFile = _PathStatementFile;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("PathStatementFile");
                }
            }
        }

        private string _PathSourceFile;
        public string PathSourceFile
        {
            get { return _PathSourceFile; }
            set
            {
                if (_PathSourceFile != value)
                {
                    _PathSourceFile = value;
                    Properties.Settings.Default.PathSourceFile = _PathSourceFile;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("PathSourceFile");
                }
            }
        }

        private string _PathInstructionFile;
        public string PathInstructionFile
        {
            get { return _PathInstructionFile; }
            set
            {
                if (_PathInstructionFile != value)
                {
                    _PathInstructionFile = value;
                    Properties.Settings.Default.PathInstructionFile = _PathInstructionFile;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("PathInstructionFile");
                }
            }
        }

        private string _PathFolderResult;
        public string PathFolderResult
        {
            get { return _PathFolderResult; }
            set
            {
                if (_PathFolderResult != value)
                {
                    _PathFolderResult = value;
                    Properties.Settings.Default.PathFolderResult = _PathFolderResult;
                    Properties.Settings.Default.Save();
                    OnPropertyChanged("PathFolderResult");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
